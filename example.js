const SendEmail = require(".");
const path = require('path');
const handlebars = require('handlebars');
const { promisify } = require('util');
const fs = require('fs');


(async () => {
    try {
        const attachments = [{
            filename: 'text3.txt',
            path: './xml.txt'
        }, {
            filename: 'image.jpg',
            path: 'FDH2103-0001-FC-010-1.jpg',
            cid: 'unique@nodemailer.com'
        }]

        const data = {
            "name": "Alan", "hometown": "Somewhere, TX",
            "kids": [{ "name": "Jimmy", "age": "12" }, { "name": "Sally", "age": "4" }],
            "color": "red"
        }

        const pathHTML = path.join(__dirname, './index.html')
        const readFile = promisify(fs.readFile);
        const template = handlebars.compile(await readFile(pathHTML, 'utf8'));
        const htmlToSend = template(data);
        const sendEmail = new SendEmail('"Anis Chentanomwong 👻" <zinachen1@gmail.com>');
        const res = await sendEmail.onSendEmail({ subject: 'ทดสอบ', to: 'zinachen1@gmail.com', pathHTML: htmlToSend });
        console.log(res)
    } catch (error) {
        console.log(error)
    }
})()