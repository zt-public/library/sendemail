const nodemailer = require('nodemailer');
require('./config_env')

class SendEmail {
    constructor(sender) {
        const { USER_EMAIL, PASS_EMAIL } = process.env;
        this.sender = sender;
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            port: 465,
            secure: true,
            auth: {
                user: USER_EMAIL,
                pass: PASS_EMAIL
            }
        });
    }

    async onSendEmail({ subject, to, message, pathHTML, attachments }) {
        try {
            if (!to) {
                return { success: false, error: 'to not found' }
            }

            let info = await this.transporter.sendMail({
                from: this.sender, // อีเมลผู้ส่ง
                to, // อีเมลผู้รับ สามารถกำหนดได้มากกว่า 1 อีเมล โดยขั้นด้วย ,(Comma)
                subject,
                text: message,
                html: pathHTML,
                attachments
            });

            if (!info.messageId) {
                return { success: false, error: info }
            }

            return { success: true }
        } catch (error) {
            return { success: false, error }
        }
    }
}

module.exports = SendEmail;
